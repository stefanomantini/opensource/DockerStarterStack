from behave import given, when, then, step
import requests
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

@given('rabbitmq is up')
def status_check(context):
    # add a message to the default queue
    response = requests.get(context.constants["rabbit_api_root"]+"aliveness-test/%2f")
    assert response.status_code == 200

    is_up = response.json()["status"]
    logger.debug("REST request status: "+str(response.status_code))
    logger.debug("REST request response: "+str(is_up))

    assert is_up == "ok"