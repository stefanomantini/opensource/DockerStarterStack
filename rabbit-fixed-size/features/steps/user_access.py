from behave import given, when, then, step
import requests
import logging
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

when(u'we check the <user> permissions')
def step_impl(context):
   raise NotImplementedError(u'STEP: When we check the <user> permissions')
   
@then(u'we have access to the following <vhost1> <vhost2>')
def step_impl(context):
    raise NotImplementedError(u'STEP: we have access to the following <vhost1> <vhost2>')
   

@when(u'we are logged in as {user:W} user')
def step_impl(context, user):
    raise NotImplementedError(u'STEP: we are logged in as {user:g} user')


@then(u'we can read from {queue:g} on the {vhost:W} vhost')
def step_impl(context, queue, vhost):
    raise NotImplementedError(u'STEP: we can read from {queue:g} on the {vhost:g} vhost')
    
@then(u'we can write to {queue:W} on the {vhost:W} vhost')
def step_impl(context, queue, vhost):
    raise NotImplementedError(u'STEP: we can write to {queue:g} on the {vhost:g} vhost')
    
@then(u'we can configure {queue:W} on the {vhost:W} vhost')
def step_impl(context, queue, vhost):
    raise NotImplementedError(u'STEP: we can configure {queue:g} on the {vhost:g} vhost')