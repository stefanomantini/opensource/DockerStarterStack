from behave import given, when, then, step
import requests
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
    
@then('we have {number:d} nodes in the cluster')
def node_check(context, number):
    response = requests.get(context.constants["rabbit_api_root"]+"nodes")
    assert response.status_code == 200

    cluster_status = response.json()
    logger.debug("REST request status: "+str(response.status_code))
    logger.debug("REST request response: "+str(cluster_status))

    assert len(cluster_status) == number

@then('the ha-policy is set to mirror all')
def step_impl(context):
    raise NotImplementedError(u'STEP: Then the ha-policy is set to mirror all')