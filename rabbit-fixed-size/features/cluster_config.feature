Feature: RabbitMQ Configuration

  Background: setup rabbitmq
    Given the rabbitmq-service has been started
    And i run the configuration script

  ####################################################################
  # cluster configuration
  ####################################################################

  Scenario: Check cluster has two nodes
    Given rabbitmq is up
      Then we have 2 nodes in the cluster
      
  Scenario: Check default vhost is reachable
    Given rabbitmq is up
      Then we can write to the %2F vhost
      
  Scenario: Check ADMIN vhost is reachable
    Given rabbitmq is up
      Then we can write to the ADMIN vhost

  Scenario: HA-Policy is set to mirror all
    Given rabbitmq is up
      Then the ha-policy is set to mirror all

      
  ####################################################################
  # user configuration
  ####################################################################
  
  Scenario Outline: Check admin user can see the following vhosts (%2F ADMIN)
    Given rabbitmq is up
    When we check the global <user> permissions
    Then we have the corresponding <tags>
        | user     | tags          |
        | admin    | administrator |
        | ADMIN     | administrator |
        | APP     |               |
        
  Scenario: Check VHost access
    Given rabbitmq is up
    When we check the <user> permissions
    Then we have access to the following <vhost1> <vhost2>
        | user     | vhost1          | vhost2   |
        | admin    | %2F             | ADMIN     |
        | ADMIN     | %2F             | ADMIN     |
        | APP     | ADMIN            |          |
        

  Scenario: Check xfer user has sufficient privileges
    Given rabbitmq is up
      When we are logged in as xfer user
      Then we can write to Q1 on the ADMIN vhost
      
  Scenario: Check APP user has sufficient privileges
    Given rabbitmq is up
      When we are logged in as APP user
      Then we can read from Q1 on the ADMIN vhost
        And we can write to Q1 on the ADMIN vhost
        And we can read from Q2 on the ADMIN vhost
        And we can write to Q2 on the ADMIN vhost

        
  Scenario: Check ADMIN admin user has sufficient privileges
    Given rabbitmq is up
      When we are logged in as ADMIN user
      Then we can write to aliveness-test on the %2F vhost
        And we can write to aliveness-test on the ADMIN vhost
        And we can read from Q1 on the ADMIN vhost
        And we can configure Q1 on the ADMIN vhost
        And we can write to Q1 on the ADMIN vhost
        And we can read from Q2 on the ADMIN vhost
        And we can write to Q2 on the ADMIN vhost
        And we can configure Q2 on the ADMIN vhost

  Scenario: Check the guest user is removed
    Given rabbitmq is up
      Then the guest user does not exist
      
