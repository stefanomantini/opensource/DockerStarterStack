#!/bin/sh

sleep 20

echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Configuration starting" 

## VARIABLES
app_vhost="microservice"
app_max_connections="3"
app_max_queues="12"
app_username="application"
admin_username="admin"

app_password="changeit"
admin_password="changeit"

## VHOST

# create VHost
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Attempting to add vhost: ${app_vhost}"
rabbitmqctl add_vhost ${app_vhost}
rabbitmqctl list_vhosts | grep ${app_vhost}

## USERS


# App User
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Adding User: ${app_username} to VHost: ${app_vhost}"
rabbitmqctl add_user ${app_username} ${app_password}
rabbitmqctl list_users | grep ${app_username}

# Admin User (Admin for all queues)
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Adding User: ${admin_username} to VHost: ${app_vhost}"
rabbitmqctl add_user ${admin_username} ${admin_password}
rabbitmqctl list_users | grep ${admin_username}

## USER PERMISSIONS


# App only has access to queues starting with prefix
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Adding Permissions to User: ${app_username} On VHost ${app_vhost}"
rabbitmqctl set_permissions -p ${app_vhost} ${app_username} "(PREFIX_|OTHER).*" "(PREFIX_|OTHER).*" "(PREFIX_|OTHER).*"
rabbitmqctl list_permissions -p ${app_vhost} | grep ${app_username}

# admin User has access to all queues on the VHost queues
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Adding Permissions to User: ${admin_username} & tagging as ADMIN On VHost ${app_vhost}"
rabbitmqctl set_permissions -p ${app_vhost} ${admin_username} ".*" ".*" ".*"
rabbitmqctl set_user_tags ${admin_username} administrator
rabbitmqctl list_permissions -p ${app_vhost} | grep ${admin_username}

## SECURITY

# change guest account password
#echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Changing guest account password"
#rabbitmqctl change_password guest ${guest_password}

# remove guest account
#guest_user="guest"
#echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Deleting default user: guest_user from VHost ${app_vhost}"
#rabbitmqctl delete_user ${guest_user}

# remove default vhost ()
#echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Deleting default vhost: / "
#rabbitmqctl delete_vhost '/'

## POLICIES

# Queue Mirroring
# This setting is very conservative, mirroring to a quorum (N/2 + 1) of cluster nodes is
# recommended instead. Mirroring to all nodes will put additional strain on all cluster
# nodes, including network I/O, disk I/O and disk space usage.
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Setting Mirroring policy across all nodes"
rabbitmqctl set_policy ha-all -p ${app_vhost} "^.*" '{"ha-mode":"all"}'
rabbitmqctl list_policies

rabbitmqctl list_queues
rabbitmqctl list_exchanges
echo "[INFO] $(date "+%Y-%m-%d %H:%M:%S") | Configuration completed"


