#!/bin/bash

set -eu

# Requires openssl installed
# apt-get update
# apt-get install openssl -y

export CERT_DIR=./rabbitcerts
USEHOSTNAME="rabbit"
CERTPASS="bbsipass123!"

if [ -d "$CERT_DIR" ]; then
  echo "ERROR: [$CERT_DIR] already exists. Remove and retry"
  exit 1
fi

mkdir -p $CERT_DIR/certs
mkdir -p $CERT_DIR/private
chmod 700 $CERT_DIR/private
mkdir -p $CERT_DIR/server
mkdir -p $CERT_DIR/client
echo 01 > $CERT_DIR/serial
touch $CERT_DIR/index.txt
cp openssl.cnf $CERT_DIR

#
# Prepare the certificate authority (self-signed).
#
cd $CERT_DIR
CERT_DIR=`pwd`

# Create a self-signed certificate that will serve a certificate authority (CA).
# The private key is located under "private".
openssl req -x509 -config openssl.cnf -newkey rsa:2048 -days 365 -out cacert.pem -outform PEM -subj /CN=MyTestCA/ -nodes

# Encode our certificate with DER.
openssl x509 -in cacert.pem -out cacert.cer -outform DER

#
# Prepare the server's stuff.
#
cd $CERT_DIR/server

# Generate a private RSA key.
openssl genrsa -out key.pem 2048

# Generate a certificate from our private key.
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=${USEHOSTNAME}/O=server/ -nodes

# Sign the certificate with our CA.
cd $CERT_DIR
openssl ca -config openssl.cnf -in $CERT_DIR/server/req.pem -out $CERT_DIR/server/cert.pem -notext -batch -extensions server_ca_extensions

# Create a key store that will contain our certificate.
cd $CERT_DIR/server
openssl pkcs12 -export -out keycert.p12 -in cert.pem -inkey key.pem -passout pass:$CERTPASS

#
# Prepare the client's stuff.
#
cd $CERT_DIR/client

# Generate a private RSA key.
openssl genrsa -out key.pem 2048

# Generate a certificate from our private key.
openssl req -new -key key.pem -out req.pem -outform PEM -subj /CN=${USEHOSTNAME}/O=client/ -nodes

# Sign the certificate with our CA.
cd $CERT_DIR
openssl ca -config openssl.cnf -in $CERT_DIR/client/req.pem -out $CERT_DIR/client/cert.pem -notext -batch -extensions client_ca_extensions

# Create a key store that will contain our certificate.
cd $CERT_DIR/client
openssl pkcs12 -export -out key-store.p12 -in cert.pem -inkey key.pem -passout pass:$CERTPASS

# Create a trust store that will contain the certificate of our CA.
openssl pkcs12 -export -out trust-store.p12 -in $CERT_DIR/cacert.pem -inkey $CERT_DIR/private/cakey.pem -passout pass:$CERTPASS
