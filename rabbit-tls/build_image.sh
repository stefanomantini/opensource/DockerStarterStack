#!/bin/bash

IMAGE_NAME="rabbitmq-with-tls"

docker build --no-cache=true -t ${IMAGE_NAME} .
