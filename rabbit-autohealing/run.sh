docker service create --name rabbit-1 --env RABBITMQ_SETUP_DELAY=120--env RABBITMQ_USER=admin--env RABBITMQ_PASSWORD=adminpwd--env RABBITMQ_CLUSTER_NODES='rabbit@rabbit-2 rabbit@rabbit'--constraint node.labels.rabbitmq==1 --mount type=bind,source=/data/rabbitmq-1,target=/var/lib/rabbitmq--env RABBITMQ_NODENAME=rabbit@rabbit-1--env RABBITMQ_ERLANG_COOKIE=a-little-secret--env RABBITMQ_FIREHOSE_QUEUENAME=trace--env RABBITMQ_FIREHOSE_ROUTINGKEY=publish.#kuznero/rabbitmq:3.6.6-cluster

# docker service create \
    # --name rabbit-2 \
    # --env RABBITMQ_SETUP_DELAY=60 \
    # --env RABBITMQ_USER=admin \
    # --env RABBITMQ_PASSWORD=adminpwd \
    # --env RABBITMQ_CLUSTER_NODES='rabbit@rabbit-1 rabbit@rabbit' \
    # --constraint node.labels.rabbitmq==2 \
# #    --mount type=bind,source=/data/rabbitmq-2,target=/var/lib/rabbitmq \
    # --env RABBITMQ_NODENAME=rabbit@rabbit-2 \
    # --env RABBITMQ_ERLANG_COOKIE=a-little-secret \
    # --env RABBITMQ_FIREHOSE_QUEUENAME=trace \
    # --env RABBITMQ_FIREHOSE_ROUTINGKEY=publish.# \
    # kuznero/rabbitmq:3.6.6-cluster

# docker service create \
    # --name rabbit \
    # #-p #{HTTP_UI_PORT}:15672 \
    # --env RABBITMQ_SETUP_DELAY=20 \
    # --env RABBITMQ_USER=admin \
    # --env RABBITMQ_PASSWORD=adminpwd \
    # --env RABBITMQ_CLUSTER_NODES='rabbit@rabbit-1 rabbit@rabbit-2' \
    # --constraint node.labels.rabbitmq==3 \
# #    --mount type=bind,source=/data/rabbitmq-3,target=/var/lib/rabbitmq \
    # --env RABBITMQ_NODENAME=rabbit@rabbit \
    # --env RABBITMQ_ERLANG_COOKIE=a-little-secret \
    # --env RABBITMQ_FIREHOSE_QUEUENAME=trace \
    # --env RABBITMQ_FIREHOSE_ROUTINGKEY=publish.# \
    # kuznero/rabbitmq:3.6.6-cluster
